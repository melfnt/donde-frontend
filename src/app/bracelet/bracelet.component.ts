import { Component, OnInit } from '@angular/core';
import { BraceletService } from './bracelet.service';
import { Observable } from 'rxjs';
import { tap, share, switchMap, pluck, filter, distinctUntilChanged } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { Card, Phases, Status } from '@app/models';
import {MatSnackBar} from '@angular/material/snack-bar';
import { CorpusService } from '@app/corpus/corpus.service';

@Component({
  selector: 'app-bracelet',
  templateUrl: './bracelet.component.html',
  styleUrls: ['./bracelet.component.scss']
})
export class BraceletComponent implements OnInit {

  _SENT_MAX_LEN = 10;

  cards$: Observable<Card[]>;
  cards: Card[];

  selected_card: Card | null;
  selected_sentence_card: Card | null;
  
  status$: Observable<Status>;
  sentences$: Observable<Card[][]>;
  sentences: Card[][] = [];
  phase: Phases;
  
  user_id: string | undefined;
  stanza_id: string | undefined;

  constructor( private braceletService: BraceletService, private corpusService: CorpusService,
               private activatedRoute: ActivatedRoute, private _snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    
    this.cards$ = this.activatedRoute.queryParams.pipe (
      tap ( params => {
        this.user_id = params["user_id"];
        this.stanza_id = params["stanza_id"];
      }),
      switchMap ( params => {
        const user_id = params["user_id"];
        const stanza_id = params["stanza_id"];
        return this.braceletService.get_cards (user_id, stanza_id);
      }),
      share()
    );
    this.cards$.subscribe ( c => this.cards = c );
    
    this.status$ = this.activatedRoute.queryParams.pipe (
      switchMap ( params => {
        const user_id = params["user_id"];
        const stanza_id = params["stanza_id"];
        return this.braceletService.get_status (user_id, stanza_id);
      }),
      share()
    );

    this.sentences$ = this.status$.pipe (
      pluck("sentences"),
      filter ( new_sentences => {
        // this.sentences$ only emits if the sentences changed since the last server call 
        if (new_sentences.length != this.sentences.length) return true;
        const a = new_sentences.map ( s => s.length );
        const b = this.sentences.map ( s => s.length );
        for ( let j=0; j<a.length; ++j ) {
          if (a[j] != b[j]) return true;
        }
        return false;
      }),
      tap (s => this.sentences = s)
    );

    this.status$.pipe (
      pluck ("error"),
      filter (e => e)
    ).subscribe ( e => {
      console.debug ("[Bracelet-Component] there was an error");
      this._snackBar.open("Qualcun altro ha già messo una carta qui!", "", {
        duration: 4000,
      });
    });

    this.status$.pipe (
      pluck ("phase"),
      distinctUntilChanged (),
    ).subscribe (p => {
      this.phase = p
      tap (p => console.debug ("[BRACELET-COMPONENT] phase changed:", p));
      if ( p == 2 ) {
        this.corpusService.set_showing_cleartext();
      }
      if ( p == 3 ) {
        this.corpusService.set_showing_annotated_corpus();
      }
    });

  }

  allow_drop ($event: DragEvent) {
    if (this.phase == 0) {
      $event.preventDefault();
    }
  }

  drop_card ($event: DragEvent|MouseEvent, sentence_id: number, position: number) {
    if (this.phase==0 && this.selected_card) {
      const card_id = this.selected_card.id;
      console.debug ("[Bracelet-component] drop_card card id:", card_id);
      console.debug ("[Bracelet-component] drop_card sentence:", sentence_id, "position:", position);
      $event.preventDefault();
      const a = position == 0 ? "" : this.sentences[sentence_id][position-1].text;
      const b = this.selected_card.text;
      if (this.corpusService.is_valid_bigram (a,b)) {
        this.braceletService.move (card_id, sentence_id, position, this.user_id, this.stanza_id);
        this.selected_card = null;
        this.corpusService.set_highlight ("");  
      }
      else {
        this._snackBar.open ("Questo bigramma non esiste nel corpus!", "", {
          duration: 4000,
        });
      }
    }
  }
  
  drag_end ($event: DragEvent) {
    console.debug ("[Bracelet-component] drag_end $event:", $event);
    this.selected_card = null;
    this.corpusService.set_highlight ("");
  }

  drag_start ($event: DragEvent|null, c: Card) {
    console.debug ("[Bracelet-component] dragging started $event:", $event, "card:", c);
    if ($event !== null) {
      $event.dataTransfer.setData('text/plain', ''+c.id);
    }
    this.selected_card = c;
    this.selected_sentence_card = null;
    this.corpusService.set_highlight (c.text);
  }

  sentence_card_clicked (c: Card) {
    this.corpusService.set_highlight (c.text);
    this.selected_sentence_card = c;
    this.selected_card = null;
  }
  
}
