import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, merge, interval, Subject } from 'rxjs';
import { startWith, switchMap, share, filter, map } from 'rxjs/operators';
import { Card, Status } from '@app/models';
import { endpoints, environment } from '@env/environment';
import { build_params_with_ids } from '@app/utils';

interface CardsResponse {
  code: number,
  cards: {id:number,ding:string,text:string}[],
  user_nickname: string,
};

interface StatusResponse {
  code: number,
  sentences: {id:string, ding:string, token:string,user_id:string,user_nickname:string}[][],
  actions: {time: string, message: string}[],
  stato: number,
}

function convert_cards_response ( inp: CardsResponse ): Card[] {
  return inp.cards.
  filter (
    c => c.text
  ).map (
    c => ({id: c.id, text: c.ding, cleartext: c.text, owner:inp.user_nickname})
  ).sort ( (a,b) => a.text.localeCompare(b.text) );
}

function convert_status_response ( inp: StatusResponse ): Status {
  return {
    phase: inp.stato,
    error: inp.code !== 200,
    sentences: inp.sentences.map (
      s => s.map (c => ({id: +c.id, cleartext: c.token, text: c.ding, owner:c.user_nickname}))
    ),
  }
}

@Injectable({
  providedIn: 'root'
})
export class BraceletService {

  private _LOAD_SENTENCES_EVERY_MS = 2_000;
  private moves_results$ = new Subject<Status> ();
  private periodic_refresh$: Observable<Status>;
  private disable_periodic_refresh=false;

  constructor( private service: HttpClient ) {
    // this.moves_results$.subscribe (x => console.log ("[BRACELET-SERVICE] moves_result emitted", x));
  }

  public get_cards (user_id?: string, stanza_id?: string): Observable<Card[]> {
    return this.service.get <CardsResponse> (endpoints.getCards, {
      params: build_params_with_ids (user_id, stanza_id),
      withCredentials: environment.production
    }).pipe (
      map ( convert_cards_response  )
    );
  }
 
  private fetch_status_from_server ( user_id?: string, stanza_id?: string ) {
    return this.service.get <StatusResponse> ( endpoints.getStatus, {
      params: build_params_with_ids (user_id, stanza_id),
      withCredentials: environment.production
    }).pipe (
      map (convert_status_response)
    )
  }

  private post_move ( card_id: number, sentence_id: number, position: number, user_id?: string, stanza_id?: string ): Observable<Status> {
    const params = build_params_with_ids (user_id, stanza_id);
    params["carta_id"] = card_id;
    params["sentence_id"] = sentence_id;
    params["token_id"] = position;
    return this.service.get <StatusResponse> (endpoints.move, {
      params,
      withCredentials: environment.production,
    }).pipe (
      map (convert_status_response),
      // DEBUG: insert a random error once a while
      // map ( s => ({...s, error: Math.random() > 0.8}))
    );
  }

  move ( card_id: number, sentence_id: number, position: number, user_id?: string, stanza_id?: string ) {
    this.disable_periodic_refresh = true;
    this.post_move (card_id, sentence_id, position, user_id, stanza_id).subscribe ( new_status => {
      this.disable_periodic_refresh = false;
      this.moves_results$.next (new_status);
    });
  }

  public get_status ( user_id?: string, stanza_id?: string ): Observable<Status> {
    
    this.periodic_refresh$ = interval (this._LOAD_SENTENCES_EVERY_MS).pipe (
      filter ( _ => !this.disable_periodic_refresh ),
      startWith (0),
      switchMap ( _ => this.fetch_status_from_server (user_id, stanza_id) ),
      // tap (x => console.log("[BRACELET-SERVICE] periodic refresh emitted", x)),
      share (),
    );
    
    return merge (
      this.moves_results$, this.periodic_refresh$
    ).pipe (
      // tap ( x => console.log ("[BRACELET-SERVICE] sentences$ emitted", x)),
      share ()
    );
  }

}
