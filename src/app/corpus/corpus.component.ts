import { Component, OnInit } from '@angular/core';

import { CorpusService } from './corpus.service';
import { Observable } from 'rxjs';

import { ActivatedRoute } from '@angular/router';
import { CorpusSentence } from '@app/models';
import { switchMap, tap } from 'rxjs/operators';

import { environment } from '@env/environment';

function automatic_download (url: string) {
  // const a = document.createElement("a");
  // document.body.appendChild(a);
  // a.style.display = "none";
  // a.href = url;
  // a.target = "_blank";
  // a.download = "corpus_annotato.pdf";
  // a.click();
  // window.URL.revokeObjectURL(url);
  window.location.href = url;
}

@Component({
  selector: 'app-corpus',
  templateUrl: './corpus.component.html',
  styleUrls: ['./corpus.component.scss']
})
export class CorpusComponent implements OnInit {

  corpus$: Observable<CorpusSentence[]>;
  highlight$: Observable<string>;
  show_cleartext$: Observable<boolean>;

  annotated_url: string;
  show_annotated$: Observable<boolean>;

  constructor(private service: CorpusService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.annotated_url = environment.production ? "/assets/corpus.pdf" : "http://donde.altervista.org/assets/corpus.pdf";
    this.corpus$ = this.activatedRoute.queryParams.pipe (
      switchMap ( params => {
        const user_id = params["user_id"];
        const stanza_id = params["stanza_id"];
        return this.service.get_corpus (user_id, stanza_id);
      }),
      tap ( c => console.debug("[Corpus-component] corpus$ emitted ", c)),
    );

    // FIXME: cleartext is not shown if a player enters the game when status is already == 2
    this.highlight$ = this.service.get_highlight();
    this.show_cleartext$ = this.service.is_showing_cleartext();    
    this.show_annotated$ = this.service.is_showing_annotated().pipe (
      tap ( x => {
        console.log ("show annotated emitted", x);
        if (x) {
          automatic_download (this.annotated_url);
        }
      })
    );
  }

}
