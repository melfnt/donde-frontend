import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { CorpusSentence } from '@app/models';
import { endpoints, environment } from '@env/environment';
import { build_params_with_ids } from '@app/utils';
import { map, share, tap } from 'rxjs/operators';

interface CorpusResponse {
  code: number,
  phrases: {id: string, text: string, ding: string}[],
  actions: {time: string, message: string}[],
  stato: number,
}

function convert_corpus_response (inp: CorpusResponse): CorpusSentence[] {
  return inp.phrases.map ( s => ({
    cleartext: s.text,
    text: s.ding.split (/\s/),
  }))
}

@Injectable({
  providedIn: 'root'
})
export class CorpusService {

  private _highlight$ = new Subject <string> ();
  private _showing_cleartext$ = new Subject <boolean> ();
  private _showing_annotated$ = new Subject <boolean> ();

  private _corpus: CorpusSentence[];

  constructor( private service: HttpClient ) { }

  public get_corpus (user_id?: string, stanza_id?: string): Observable<CorpusSentence[]> {
    return this.service.get <CorpusResponse> (endpoints.getCorpus, {
      params: build_params_with_ids (user_id, stanza_id),
      withCredentials: environment.production,
    }).pipe (
      map (convert_corpus_response),
      tap (c => this._corpus = c),
    );
  }

  public set_highlight ( text: string ) {
    console.debug ("[CORPUS-SERVICE] highlighting", text);
    this._highlight$.next (text);
  }

  public set_showing_cleartext (show=true) {
    this._showing_cleartext$.next (show);
  }

  public get_highlight (): Observable<string> {
    return this._highlight$;
  }

  public is_showing_cleartext (): Observable<boolean> {
    return this._showing_cleartext$;
  }

  public set_showing_annotated_corpus() {
    this._showing_cleartext$.next(false);
    this._showing_annotated$.next(true);
  }

  public is_showing_annotated (): Observable<boolean> {
    return this._showing_annotated$;
  }

  public is_valid_bigram (a: string, b: string): boolean {
    if ( !a ) {
      return this._corpus.some ( sent => sent.text[0] == b );  
    }
    else {
      return this._corpus.some ( sent => {
        for (let j=0; j<sent.text.length-1; ++j) {
          if (a === sent.text[j] && b === sent.text[j+1]) return true;
        }
      });
    }
  }


}
