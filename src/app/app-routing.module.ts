import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FirstActivityComponent } from './first-activity/first-activity.component';

const routes: Routes = [
  { path: 'bracelet', component: FirstActivityComponent },
  { path: '', redirectTo: '/bracelet', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
