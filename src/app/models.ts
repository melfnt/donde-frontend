
export enum Phases {
    GAME = 0,
    STOP = 1,
    SHOW_CLEARTEXT = 2,
    SHOW_TRANSLATED = 3
}

export interface Card {
    id: number,
    cleartext: string,
    text: string,
    owner: string,
}

// text is tokenized because it has to be higlighted while cleartext is not because we don't care
export interface CorpusSentence {
    cleartext: string,
    text: string[]
}

export interface Status {
    sentences: Card[][],
    phase: Phases,
    error: boolean
}
