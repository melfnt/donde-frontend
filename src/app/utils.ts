
export function build_params_with_ids (user_id?: string, stanza_id?: string) {
    const out = {};
    if (user_id) out["user_id"] = user_id;
    if (stanza_id) out["stanza_id"] = stanza_id;
    return out;
}
