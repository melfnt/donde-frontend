import { Component, OnInit, Input } from '@angular/core';

function string_hash (s: string)
{
	let hash = 0;
  for (let i = 0; i < s.length; ++i) 
  {
      const c = s.charCodeAt(i);
      hash = ((hash<<5)-hash)+c;
      hash = hash & hash;
  }
  return hash;
}

function random_color_from_string (s: string) {
  const possible_colors = [
    {bg: "yellow", fg: "black"},
    {bg: "red", fg: "black"},
    {bg: "blue", fg: "white"},
    {bg: "green", fg: "black"},
    {bg: "black", fg: "white"},
    {bg: "purple", fg: "white"},
  ];
  const i = ((string_hash (s) % possible_colors.length)+ possible_colors.length) % possible_colors.length;
  return possible_colors[i];
}

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() word="";
  @Input() highlight="";
  @Input() cleartext="";
  @Input() owner="";

  display_owner: string;
  badge_bg_color: string;
  badge_fg_color: string;

  constructor() { }

  ngOnInit(): void {
    this.display_owner = this.owner.substr (0,3);
    const {bg, fg} = random_color_from_string (this.owner);
    this.badge_bg_color = bg;
    this.badge_fg_color = fg;
  }

}
