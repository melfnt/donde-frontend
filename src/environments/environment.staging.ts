// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
};

const apiBaseUrl = "https://donde.altervista.org";

export const endpoints = {
  getCards: apiBaseUrl + "/API_get_user_card.php",
  getCorpus: apiBaseUrl + "/API_get_phrases.php",
  getStatus: apiBaseUrl + "/API_get_sentences.php",
  move: apiBaseUrl + "/API_move_card.php",
}

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
